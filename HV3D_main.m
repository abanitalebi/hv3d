
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HV3D: Human Visual System-Based 3D Quality Metric for Stereoscopic Video
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a first implementation of HV3D video quality metric
% Please refer to the following paper for details regarding the metric:

% A. Banitalebi-Dehkordi, M.T. Pourazad, and Panos Nasiopoulos, 
% "An Efficient Human Visual System Based Quality Metric for 3D Video,"
% Springer Journal of Multimedia Tools and Applications, pp. 1-29, Feb. 2015.

% Note that this implementation is slightly different than the method 
% of the paper. The results however are close enough to the results of the
% paper. Next release will provide the actual implementation of the paper. 

% Note that this implementation uses the matlabPyrTools. So make sure you
% have matlabPyrTools working properly.

% Please cite the above paper in case you use this code.

% This code is provided for free for non-commercial use only. 
% January 2016, Amin Banitalebi, dehkordi@ece.ubc.ca


clear all;
close all;
clc;
addpath(genpath('files')); warning off all;

%%%%%%%%%%%%%%%%%%%%%%% Configuration Parameters %%%%%%%%%%%%%%%%%%%%%
nof = 5;                  % Number of Frames
width = 480;              % Width
height = 270;             % Height
format = '420';           % Chroma Sub-sampling

min_disparity_range = -8; % minimum disparity search range used in DERS
max_disparity_range = 4;  % maximum disparity search range used in DERS

%%%%%%%%%%%%%%%%%%%%%%%%% Reference Video %%%%%%%%%%%%%%%%%%%%%%%
FileName_ref_left = 'D:\left_480x270.yuv';
FileName_ref_right = 'D:\right_480x270.yuv';
FileName_ref_depth = 'D:\depth_480x270.yuv';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Distorted %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FileName_dist_left = 'D:\Distorted\compression\left_480x270_compressed_QP40.yuv';
FileName_dist_right = 'D:\Distorted\compression\right_480x270_compressed_QP40.yuv';
FileName_dist_depth = 'D:\Distorted\compression\depth_480x270_compressed_QP40.yuv';


%%%%%%%%%%%%%%%%%%%% Parameters: do not change these! %%%%%%%%%%%%%%%%%
bwidth=16; % block width  
bheight=16;  % block height  

vwidth = 64;  % variance block width  
vheight = 64; % variance block height 

swidth = 64;  % searching area width  
sheight = 64;  % searching area height  

Tc = JpegQTn();

min_disp = -max_disparity_range; % minimum disparity
max_disp = -min_disparity_range; % maximum disparity


%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Main Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fwidth = 0.5;
fheight= 0.5;
if strcmp(format,'400')
    fwidth = 0;
    fheight= 0;
elseif strcmp(format,'411')
    fwidth = 0.25;
    fheight= 1;
elseif strcmp(format,'420')
    fwidth = 0.5;
    fheight= 0.5;
elseif strcmp(format,'422')
    fwidth = 0.5;
    fheight= 1;
elseif strcmp(format,'444')
    fwidth = 1;
    fheight= 1;
else
    display('Error: wrong format');
end

for i = 1:nof
    
    YUV_ref_1 = loadFileYUV(width,height,i,FileName_ref_left,fheight,fwidth);
    YUV_ref_2 = loadFileYUV(width,height,i,FileName_ref_right,fheight,fwidth);
    YUV_ref_depth = loadFileYUV(width,height,i,FileName_ref_depth,fheight,fwidth);
    
    YUV_dist_1 = loadFileYUV(width,height,i,FileName_dist_left,fheight,fwidth);
    YUV_dist_2 = loadFileYUV(width,height,i,FileName_dist_right,fheight,fwidth);
    YUV_dist_depth = loadFileYUV(width,height,i,FileName_dist_depth,fheight,fwidth);
    
    RGB_ref_1 = ycbcr2rgb(YUV_ref_1); 
    RGB_ref_2 = ycbcr2rgb(YUV_ref_2); 
    RGB_ref_depth = ycbcr2rgb(YUV_ref_depth); 
    
    RGB_dist_1 = ycbcr2rgb(YUV_dist_1); 
    RGB_dist_2 = ycbcr2rgb(YUV_dist_2); 
    RGB_dist_depth = ycbcr2rgb(YUV_dist_depth); 
 
    [HV3D_all(i),time_HV3D_all(i),Q(:,i),VIFd(i)] = HV3D_index(width,height,min_disp,max_disp,bwidth,bheight,vwidth,vheight,swidth,sheight,Tc,RGB_ref_1,RGB_ref_2,RGB_dist_1,RGB_dist_2,RGB_ref_depth,RGB_dist_depth,YUV_ref_1,YUV_ref_2,YUV_dist_1,YUV_dist_2);
    
    clc;
    disp('percent completed:');  
    disp(i*100/nof);
    
end

HV3D = Temporal_Pooling (HV3D_all, 1, '', '')
