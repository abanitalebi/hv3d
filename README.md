# HV3D:
## Human Visual System based quality metric for 3D video.	

HV3D is an efficinet full-reference quality metric for quality assessment of stereoscopic 3D video.                                                                                          

Disclaimer: This code is free for use only for research purposes. For any commercial use please contact us at: amin [dot] banitalebi [at] gmail [dot] com
 
Please cite the following reference paper if you need to cite this code:
 
[1] A. Banitalebi-Dehkordi, M. T. Pourazad, and P. Nasiopoulos, "An Efficient Human Visual System Based Quality Metric for 3D Video," Springer Journal of Multimedia Tools and Applications, pp. 1-29, Feb. 2015, DOI: 10.1007/s11042-015-2466-z.  

    camera ready: http://ece.ubc.ca/~dehkordi/Files/papers/2015_MToolsApplications_Dehkordi_An%20Efficient%20Human%20Visual%20System%20Based.pdf

    published version: http://link.springer.com/article/10.1007/s11042-015-2466-z

    arxiv: https://arxiv.org/abs/1803.04832

 
